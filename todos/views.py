from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list")
    else:
        form = TodoListForm()
        context = {
            "form": form,
        }
        return render(request, "todos/create.html", context)


def todo_list_edit(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todo)

    context = {
        "todo_object": todo,
        "form": form,
    }

    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todos = TodoList.objects.get(id=id)
    if request.method == "POST":
        todos.delete()
        return redirect("todo_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form_item = TodoItemForm(request.POST)
        if form_item.is_valid():
            todo_item = form_item.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form_item = TodoItemForm()
        context = {
            "form_item": form_item,
        }
        return render(request, "todos/item_create.html", context)


def todo_item_update(request, id):
    todo = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form_item = TodoItem(request.POST, instance=todo)
        if form_item.is_valid():
            form_item.save()
            return redirect("todo_list_detail", id=id)
    else:
        form_item = TodoItemForm(instance=todo)

    context = {
        "form_item": form_item,
    }

    return render(request, "todos/item_edit.html", context)
