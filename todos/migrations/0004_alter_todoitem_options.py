# Generated by Django 4.2.1 on 2023-05-31 23:03

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0003_alter_todoitem_options"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="todoitem",
            options={},
        ),
    ]
